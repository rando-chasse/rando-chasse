CREATE TABLE IF NOT EXISTS chasseur(
    num_permis TEXT NOT NULL,
    nom_chasseur TEXT NOT NULL,
    mdp TEXT NOT NULL,
    diane TEXT NOT NULL,
    FOREIGN KEY (num_permis)
    REFERENCES verif(num_permis)
    FOREIGN KEY (diane)
    REFERENCES diane(id_diane),
    PRIMARY KEY (num_permis)
);


CREATE TABLE IF NOT EXISTS verif(
    num_permis TEXT PRIMARY KEY,
    chasseur TEXT NOT NULL,
    diane TEXT NOT NULL,
    FOREIGN KEY (diane)
    REFERENCES diane(id_diane)
);

INSERT INTO verif
(num_permis, chasseur, diane)
VALUES
('A22034ZK9', 'thomas', 'montpeyroux'),
('E3465R6', 'joris', 'montpeyroux'),
('24T34I', 'keran', 'saint-jean de fos'),
('67THG8', 'enzo', 'saint-jean de fos'),
('67DF76', 'loris', 'aniane'),
('217FJ7', 'romane', 'aniane');


CREATE TABLE IF NOT EXISTS diane(
    id_diane INT PRIMARY KEY,
    nom_diane TEXT NOT NULL
);

INSERT INTO diane
(id_diane, nom_diane)
VALUES
(1, 'montpeyroux'),
(2, 'saint-jean de fos'),
(3, 'aniane');

CREATE TABLE IF NOT EXISTS zone_chasse(
    id_zone TEXT PRIMARY KEY,
    nom_zone TEXT NOT NULL, 
    id_diane TEXT NOT NULL,
    FOREIGN KEY (id_diane)
        REFERENCES diane(id_diane)  
);

INSERT INTO zone_chasse
VALUES
('rou', 'rouveyrade', 'montpeyroux'),
('bart', 'bartassade', 'saint-jean de fos'),
('clap', 'claparede', 'aniane');

CREATE TABLE IF NOT EXISTS zone_chasse_reserve(
    id_zone_chasse TEXT,
    date TEXT,
    PRIMARY KEY (id_zone, date)
    FOREIGN KEY (id_zone_chasse)
        REFERENCES zone_chasse(id_zone) 
);

INSERT INTO zone_chasse_reserve
VALUES
('rou', '2024-03-24'),
('bart', '2024-03-25'),
('rou', '2024-03-26'),
('bart', '2024-03-27');
