clear_sel = (elm_select) => {
    let i = elm_select.length
    while (i > 0){
        elm_select.remove(i-1)
        i -= 1
    }
}

build_sel = (arr, elm_sel) => {
    clear_sel(elm_sel)
    for (let i = 0; i < arr.length; i++) {
        let optn = arr[i]
        let el = document.createElement("option")
        el.textContent = optn
        el.value = optn
        elm_sel.appendChild(el)
        }
}

/**
 
callback associé à l'event 'change' sur elm_sel1
si l'<option> vide est select, on vide elm_sel2
sinon, on envoie au server la value select sur le socket
*/
transfer_data = (elm_sel1, elm_sel2) => {
    if (elm_sel1.selectedOptions[0].value === ''){
        clear_sel(elm_sel2)
    } else {
        console.log("send msg to server")
        socket.emit("choosediane", {data: elm_sel1.selectedOptions[0].value})
    }
}

function get_type() {// fonction qui recupere le type
    type = document.getElementById("type").textContent
    if (type === "1" ){
        alert("Mot de passe incorect !");
    }
    if (type === "2" ){
        alert("Information incorect !");
    }
    if (type === "3" ){
        alert("Connexion réussi !");
    }
}


window.addEventListener("load", (event) => {// fonction qui regarde si type == 1 et qui renvoie une alerte
    console.log("La page est complètement chargée")
    get_type()
  });



  ///main

let rep
const diane = document.getElementById("diane")// menu déroulant diane
const select = document.getElementById("sn") // menu déroulant nom chasseur diane
const socket = io()
socket.on('connect', () => {socket.emit("greet", {data: "Hi!"})})
socket.on('db_chasseur', (msg) => {build_sel(msg, select)})
diane.addEventListener('change', () => transfer_data(diane, select))