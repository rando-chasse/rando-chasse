document.getElementById("macarte")


src="https://unpkg.com/leaflet@1.9.4/dist/leaflet.js"
integrity="sha256-20nQCchB9co0qIjJZRGuk2/Z9VM+kNiyxNV1lvTlZBo="
crossorigin=""
    
function onMapClick(e) {
    console.log(e.target.id)
    popup
        .setLatLng(e.latlng)
        .setContent("You clicked the map at " + e.getLayerId)
        .openOn(carte)
}

const carte = L.map('macarte').setView([43.695658, 3.505335], 13)
L.tileLayer('https://tile.openstreetmap.org/{z}/{x}/{y}.png', {
    maxZoom: 19,
    attribution: '&copy <a href="http://www.openstreetmap.org/copyright">OpenStreetMap</a>'
 }).addTo(carte)


function get_type() {
    type = document.getElementById("type").textContent
    if (type === '1'){
    a = document.getElementById("parcelle_chasser").textContent
    heure = document.getElementById("h").textContent
    date = document.getElementById("date").textContent
    alert("zone "+ a + " reserver a "+heure+" le "+date+"!");
    }
}


window.addEventListener("load", (event) => {
    console.log("La page est complètement chargée")
    get_type()
  });
  

// polygon de couleur orange pour les randonnneur et chasse reserver 

const polygon1 = L.polygon([
    [43.689022, 3.496575],
    [43.684462, 3.489579],
    [43.699326, 3.478883],
    [43.703141, 3.484026]
], {color: "orange" , id: "rouveyrade"})
polygon1.addEventListener('click', (e) => {info.update(e.target.options.id)})

const polygon2 = L.polygon([
    [43.68739173292055, 3.517616438701267],
    [43.69099439167904, 3.5233278086814948],
    [43.69740834601864, 3.518831623803443],
    [43.6918730561112, 3.511054439149516]
], {color: "orange", id: "bartassade"})
polygon2.addEventListener('click', (e) => {info.update(e.target.options.id)})

// polygon de couleur verte pour les chasses non reserver 

const polygon3 = L.polygon([
    [43.689022, 3.496575],
    [43.684462, 3.489579],
    [43.699326, 3.478883],
    [43.703141, 3.484026]
], {color: "green" , id: "rouveyrade"})
polygon3.addEventListener('click', (e) => {info.update(e.target.options.id)})

const polygon4 = L.polygon([
    [43.68739173292055, 3.517616438701267],
    [43.69099439167904, 3.5233278086814948],
    [43.69740834601864, 3.518831623803443],
    [43.6918730561112, 3.511054439149516]
], {color: "green", id: "bartassade"})
polygon4.addEventListener('click', (e) => {info.update(e.target.options.id)})

const parcelle = {
    "rouveyrade": polygon1,
    "bartassade": polygon2
}

const parcelle_non_chasse = {
    "rouveyrade": polygon3,
    "bartassade": polygon4
}

affiche_parcelle = (id) => {parcelle[id].addTo(carte)}
affiche_parcelle_non_chasse = (id) => {parcelle_non_chasse[id].addTo(carte)}

function sendtohtml (zone) {
    const zone_chasse = zone
    document.querySelector("#zone_selec").innerHTML = zone_chasse
    document.getElementById("result").setAttribute('value', zone_chasse)
    
}

const socket = io()
socket.on('connect', () => {socket.emit("get_parcelle", {data: "envoie l'id de la parcelle chassée"})})
socket.on('send_parcelle', (msg) => {affiche_parcelle(msg['id'])})
socket.on('send_parcelle_non_chasse', (msg) => {affiche_parcelle_non_chasse(msg['id'])})


const info = L.control()
info.onAdd = function (carte) {
    this._div = L.DomUtil.create('div', 'info')
    this.update()
    return this._div
}

info.update = function (parcelle) {
    const contents = parcelle ? `${parcelle}` : 'Cliquer sur une parcelle pour la choisir'
    this._div.innerHTML = `<h4>Diane de Montpeyroux</h4>${contents}`;
    const par = contents
    sendtohtml(par)
}

info.addTo(carte)