document.getElementById("macarte")
const carte = L.map('macarte').setView([43.695658, 3.505335], 13)
L.tileLayer('https://tile.openstreetmap.org/{z}/{x}/{y}.png', {
    maxZoom: 19,
    attribution: '&copy <a href="http://www.openstreetmap.org/copyright">OpenStreetMap</a>'
 }).addTo(carte)

// polygon de couleur orange pour les randonnneur et chasse reserver 
/*
const polygon1 = L.polygon([
    [43.689022, 3.496575],
    [43.684462, 3.489579],
    [43.699326, 3.478883],
    [43.703141, 3.484026]
], {color: "orange" , id: "rouveyrade"})
polygon1.addEventListener('click', (e) => {info.update(e.target.options.id)})

const polygon2 = L.polygon([
    [43.68739173292055, 3.517616438701267],
    [43.69099439167904, 3.5233278086814948],
    [43.69740834601864, 3.518831623803443],
    [43.6918730561112, 3.511054439149516]
], {color: "orange", id: "bartassade"})
polygon2.addEventListener('click', (e) => {info.update(e.target.options.id)})

const parcelle = {
    "rouveyrade": polygon1,
    "bartassade": polygon2
}
pour chaque clé dans parcelle
const p3 = L.polygon(res[x], {color: "green", id: x})
*/
let res = {}
const parcelle = {}

create_polygon = (n, d) => {L.polygon(d, {color: "green", id: n})
}


clear_parcelle = () => {
    Object.keys(parcelle).forEach(e => parcelle[e].remove())
}

affiche_parcelle = (id) => {
    parcelle[id].addTo(carte)
}


sendtohtml= (zone) => {
    const zone_chasse = zone
    document.querySelector("#zone_selec").innerHTML = zone_chasse
    document.getElementById("result").setAttribute('value', zone_chasse)
    
}

const socket = io()
const date_chasse = document.getElementById('date_chasse')
let polygs
date_chasse.addEventListener('change', ()=> {socket.emit("send_date_chasse", {'date': date_chasse.value})})

socket.on('connect', () => {socket.emit("get_json", {data: ""})})

socket.on('send_json', (msg) => {console.log(msg['data']); res = msg['data']; 
polygs = Object.keys(res).map(x=>L.polygon(res[x], {color: "green", id: x}))
polygs.forEach(p=>p.addTo(carte))
//build_parcelle(msg['data'])
polygs.forEach(p=>p.addEventListener('click', (e) => {info.update(e.target.options.id)}))
})

socket.on('send_parcelle', (msg) => {clear_parcelle() ; msg['id'].forEach(x => affiche_parcelle(x))})
const info = L.control()
info.onAdd = function (carte) {
    this._div = L.DomUtil.create('div', 'info')
    this.update()
    return this._div
}

info.update = function (parcelle) {
    const contents = parcelle ? `${parcelle}` : 'Cliquer sur une parcelle pour la choisir'
    this._div.innerHTML = `<h4>Diane de Montpeyroux</h4>${contents}`;
    const par = contents
    sendtohtml(par)
}

info.addTo(carte)