document.getElementById("macarte")


/*    
function onMapClick(e) {
    console.log(e.target.id)
    popup
        .setLatLng(e.latlng)
        .setContent("You clicked the map at " + e.getLayerId)
        .openOn(carte)
}
*/
const carte = L.map('macarte').setView([43.695658, 3.505335], 13)
L.tileLayer('https://tile.openstreetmap.org/{z}/{x}/{y}.png', {
    maxZoom: 19,
    attribution: '&copy <a href="http://www.openstreetmap.org/copyright">OpenStreetMap</a>'
 }).addTo(carte)

// polygon de couleur orange pour les randonnneur et chasse reserver 

const polygon1 = L.polygon([
    [43.689022, 3.496575],
    [43.684462, 3.489579],
    [43.699326, 3.478883],
    [43.703141, 3.484026]
], {color: "orange" , id: "rouveyrade"})
polygon1.addEventListener('click', (e) => {info.update(e.target.options.id)})

const polygon2 = L.polygon([
    [43.68739173292055, 3.517616438701267],
    [43.69099439167904, 3.5233278086814948],
    [43.69740834601864, 3.518831623803443],
    [43.6918730561112, 3.511054439149516]
], {color: "orange", id: "bartassade"})
polygon2.addEventListener('click', (e) => {info.update(e.target.options.id)})

const parcelle = {
    "rouveyrade": polygon1,
    "bartassade": polygon2
}

clear_parcelle = () => {
    Object.keys(parcelle).forEach(e => parcelle[e].remove())
}

affiche_parcelle = (id) => {
    parcelle[id].addTo(carte)
}

const socket = io()
const date_rando = document.getElementById('date_rando')
date_rando.addEventListener('change', ()=> {socket.emit("send_date", {'date': date_rando.value})})

socket.on('connect', () => {socket.emit("get_parcelle", {data: ""})})
socket.on('send_parcelle', (msg) => {clear_parcelle() ; msg['id'].forEach(x => affiche_parcelle(x))})
