from flask import Flask, render_template, request, session, redirect, url_for, flash
import json
import sqlite3
from os.path import join, dirname, realpath
from flask_socketio import SocketIO, send, emit
import datetime
from datetime import timedelta, date
from time import strftime

app = Flask(__name__)

app.config['DATA_DIR'] = join(dirname(realpath(__file__)),'static')
app.secret_key = b'99b45274a4b2da7440ab249f17e718688b53b646f3dd57f23a9b29839161749f'
socketio = SocketIO(app, logger = True, engineio_logger = True)

@socketio.on('send_date_chasse')
def handle_date_chasse(msg):
    date = msg['date']
    con = sqlite3.connect(join(app.config['DATA_DIR'], "chasseur.db"))
    cur = con.cursor()
    xs = cur.execute(
        "SELECT nom_zone FROM zone_chasse JOIN zone_chasse_reserve ON id_zone = id_zone_chasse WHERE date=?", (date,)).fetchall()
    data = [x[0] for x in xs]    
    emit('send_parcelle', {'id': data})
    con.close()

@socketio.on('get_json')
def handle_get_json(msg):
    with open(join(app.config['DATA_DIR'],"parcelle.json"), 'r') as f:
        parcelles = json.load(f)
    emit('send_json', {'data': parcelles})

@socketio.on('get_parcelle')
def handle_get_parcelle(msg):
    con = sqlite3.connect(join(app.config['DATA_DIR'], "chasseur.db"))
    cur = con.cursor()
    if session['rand'] == 1:
        session['acces_carte'] = 0
        don = request.form
        a = don.get('date_chasse')
        print(f"data de date_chasse {a}")
        b = cur.execute("""SELECT id_zone_chasse FROM zone_chasse_reserve WHERE date=?""", (a,))
        bb = b.fetchall()
        for k in bb:
            c = cur.execute("""SELECT nom_zone FROM zone_chasse JOIN zone_chasse_reserve ON zone_chasse_reserve.id_zone_chasse=zone_chasse.id_zone WHERE id_zone_chasse=?""", (k[0][0], ))
            cc = c.fetchall()
            parcelle = cc[0][0]
            emit("send_parcelle", {"id": parcelle})
    if session['acces_carte'] == 1 :
        p = cur.execute("""SELECT nom_zone FROM zone_chasse JOIN zone_chasse_reserve ON zone_chasse_reserve.id_zone_chasse=zone_chasse.id_zone""")
        pp = p.fetchall()
        f = cur.execute("""SELECT nom_zone FROM zone_chasse""")
        ff = f.fetchall()
        for i in ff :
            if i in pp:
                verif_d = cur.execute("""SELECT dates FROM zone_chasse_reserve JOIN zone_chasse ON zone_chasse.id_zone=zone_chasse_reserve.id_zone_chasse WHERE nom_zone=?""", (i,))
                verif_date = verif_d.fetchall()
                date = datetime.datetime.now()
                date_y = date.strftime("%Y-%m-%d")
                date_yy = str(date_y)
                print(date_y)
                if verif_date[0][0] == date_y:
                    cur.execute("""DELETE FROM zone_chasse_reserve WHERE date=?""", (date_yy,))
                    con.commit()
                    parcelle = i[0]
                    emit("send_parcelle_non_chasse", {"id": parcelle})
                else:    
                    parcelle = i[0]
                    emit("send_parcelle", {"id": parcelle})
            else :
                parcelle = i[0]
                emit("send_parcelle_non_chasse", {"id": parcelle})
    if session['acces_carte'] == 2 :
        p = cur.execute("""SELECT nom_zone FROM zone_chasse JOIN zone_chasse_reserve ON zone_chasse_reserve.id_zone_chasse=zone_chasse.id_zone""")
        par = p.fetchall()
        for k in par :
            parcelle = k[0]
            emit("send_parcelle", {"id": parcelle})
    con.close()

@socketio.on('choosediane')
def handle_choosediane(msg):
    """
    qd on reçoit sur le socket un msg de type 'choosediane' de la part du client
    on traite le 'data' de msg
    en réponse, on envoie un msg de type 'db' à client avec en data le fetchall de la <<requête sql>>.
    """
    diane = msg['data']
    print(f"diane: {diane}")
    con = sqlite3.connect(join(app.config['DATA_DIR'], "chasseur.db"))
    cur = con.cursor()
    x = cur.execute("SELECT chasseur FROM verif WHERE diane = ?", (diane,))
    data = x.fetchall()
    emit('db_chasseur',data)

@app.route("/")
def index():
    session['acces_carte'] = 1
    try :
        session['acces_carte']
    except:
        session['acces_carte'] = 2
    return render_template('index.html')

@app.route("/create_login_chass")
def create_login_chass():
    con = sqlite3.connect(join(app.config['DATA_DIR'], "chasseur.db"))
    cur = con.cursor()
    a = cur.execute("SELECT nom_diane FROM diane")
    b = a.fetchall()
    session['connex'] = 2
    return render_template('create_login_chass.html',diane=b)

@app.route("/deconnexion")
def deconnexion():
    session['acces_carte'] = 2
    return render_template('index.html')

@app.route("/log_chasseur")
def log_chasseur():
    con = sqlite3.connect(join(app.config['DATA_DIR'], "chasseur.db"))
    cur = con.cursor()
    name = cur.execute("SELECT nom_diane FROM diane")
    name_d = name.fetchall()
    session['connex'] = 0
    return render_template('log_chasseur.html', diane = name_d)
    
@app.route("/test", methods=['POST'])
def test():
    con = sqlite3.connect(join(app.config['DATA_DIR'], "chasseur.db"))
    cur = con.cursor()
    don = request.form
    zone = don.get('nom')
    h = don.get('horraire')
    d = don.get('date')
    id_zone = cur.execute("""SELECT id_zone FROM zone_chasse WHERE nom_zone=(?)""", (str(zone),))
    id_z = id_zone.fetchall()
    p = cur.execute("""SELECT nom_zone FROM zone_chasse JOIN zone_chasse_reserve ON zone_chasse_reserve.id_zone_chasse=zone_chasse.id_zone""")
    pp = p.fetchall()
    f = cur.execute("""SELECT nom_zone FROM zone_chasse""")
    ff = f.fetchall()
    for i in ff :
        if (zone,) in pp:
            a = cur.execute("""SELECT horraire FROM zone_chasse_reserve WHERE id_zone_chasse=(?)""", (id_z[0][0],))
            b = a.fetchall()
            c = cur.execute("""SELECT dates FROM zone_chasse_reserve WHERE id_zone_chasse=(?)""", (id_z[0][0],))
            d = c.fetchall()
            return render_template('map_interactive.html', type = 1, h = b[0][0], parcelle_chasser = zone, date = d[0][0])
        
    cur.execute("""INSERT INTO zone_chasse_reserve(id_zone_chasse, horraire, dates) VALUES(?, ?, ?)""", (id_z[0][0], h, d))
    con.commit()
    return render_template('test.html')

@app.route("/carte")
def carte():
    session['rand'] = 0
    if session['acces_carte'] == 1 or session['acces_carte'] == 2:
        return render_template('carte.html')
    else : 
        return render_template('index.html')

@app.route("/map_interactive")
def map_interactive():
    if session['acces_carte'] == 1 :
        select_d = []
        for k in range(14):
            t = date.today() + timedelta(days = k)
            l = str(t)
            select_d.append(l)
            print(select_d) 
        return render_template('map_interactive.html', select_date = select_d)
    if session['acces_carte'] == 2 :
        session['rand'] = 1
        select_d = []
        for k in range(14):
            t = date.today() + timedelta(days = k)
            l = str(t)
            select_d.append(l)
        return render_template('map_interactive_rand.html', select_date = select_d)
    
@app.route("/map_interactive_rand", methods=['POST'])
def map_interactive_rand():
    return render_template('map_interactive_rand.html')

@socketio.on('send_date')
def handle_send_date(msg):
    date = msg['date']
    con = sqlite3.connect(join(app.config['DATA_DIR'], "chasseur.db"))
    cur = con.cursor()
    xs = cur.execute(
        "SELECT nom_zone FROM zone_chasse JOIN zone_chasse_reserve ON id_zone = id_zone_chasse WHERE date=?", (date,)).fetchall()
    data = [x[0] for x in xs]    
    emit('send_parcelle', {'id': data})
    con.close()


@app.route("/chasseur", methods=['POST'])
def chasseur():
    session['accescarte'] = 1
    con = sqlite3.connect(join(app.config['DATA_DIR'], "chasseur.db"))
    cur = con.cursor()
    don = request.form
    nm = don.get('select_nom')
    mdpconec = don.get('mdp1')
    np = don.get('num_permis')
    dia = don.get('diane')
    mdp = don.get('mdp')
    if session['connex'] == 2 :
        per = con.execute("""SELECT num_permis FROM verif WHERE chasseur=(?)""", (nm,))
        perm = per.fetchall()
        if request.form['mdp'] == request.form['mdp2'] :
            if np == perm[0][0]:
                cur.execute("""INSERT INTO chasseur(num_permis, nom_chasseur, mdp, diane) VALUES(?, ?, ?, ?)""", (np, nm, mdp, dia))
                con.commit()
                return render_template('index.html')
        else:
            return render_template('create_login_chass.html')

    a = con.execute("""SELECT mdp FROM chasseur WHERE nom_chasseur=(?)""", (nm,))
    xz = a.fetchall()
    if mdpconec == xz[0][0]:
        return render_template('index.html', type = 3)
    else:
        return render_template('log_chasseur.html', type = 1)
 
if __name__ == '__main__':
    socketio.run(app, port='8080')