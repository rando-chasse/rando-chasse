# Protocole d'utilisation

## 1. Installation
Lorsque vous avez installé les fichiers dans le dossier source, et que vous avez installé les dépendances. Il y a deux moyens d'executé et de lancer le serveur

## 2. Lancement de test
Vous pouvez lancer le serveur en executant simplement le server.py, cela va lancer un serveur de test, ceci marche pour linux et Windows.

## 3. Lancer un serveur de production
Pour lancer un serveur de production, il faut suivre la [documentation officiel de flask](https://flask.palletsprojects.com/en/3.0.x/deploying/).

Nous allons vous detailler comment le faire avec gunicorn

### 1 - Ouvrir une console de commande et aller dans votre repertoire avec votre server.py

## Lancement du serveur
Installer gunicorn dans le venv avec "pip install gunicorn" ainsi que flask avec "pip install flask"
Dès que c'est fait, executer la commande "gunicorn -w 4 'hello:app"